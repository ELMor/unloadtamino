/*
 * Created on 03-may-2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package ssf.util.tamino;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author UF371231
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Logger {

    PrintWriter wrlog=null;
    File flog=null;
    
    public void log(String s){
        System.out.println(s);
        wrlog.println(s);
        System.out.flush();
        wrlog.flush();
    }
    
    public Logger(){
        try {
            flog=File.createTempFile("tamDumper",".log");
            wrlog=new PrintWriter(new FileOutputStream(flog));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.exit(-1);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }
    
    public File closeAndGetFile(){
        wrlog.close();
        return flog;
    }
}
