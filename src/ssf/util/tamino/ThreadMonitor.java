/*
 * Created on 10-may-2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package ssf.util.tamino;

/**
 * @author UF371231
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ThreadMonitor {
    public static void dumpThreads(Logger l,String threadGroup){
        Thread[] lth=new Thread[1024];
        int nth=Thread.enumerate(lth);
        for(int i=0;i<nth;i++){
            if(lth[i].getName().startsWith(threadGroup))
            l.log("    Running "+lth[i].getName());
        }
    }
    public static int countThreads(String group){
        Thread[] lth=new Thread[1024];
        int nth=Thread.enumerate(lth);
        int count=0;
        for(int i=0;i<nth;i++){
            if(lth[i].getName().startsWith(group))
                count++;
        }
        return count;
    }
}
