/*
 * Created on 02-may-2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package ssf.util.tamino;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.jar.JarOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author UF371231
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class MTZipper {
    JarOutputStream zos = null;

    byte[] buf = new byte[2 * 1024];

    Logger log = null;

    String threadGroup;
    
    int numThreadWaiting=0;

    public MTZipper(String zipname, Logger log, String tg) throws IOException {
        zos = new JarOutputStream(new FileOutputStream(zipname));
        zos.setMethod(ZipOutputStream.DEFLATED);
        threadGroup = tg;
        this.log = log;
    }

    public void addEntry(String name, String contenido) throws IOException {
        runThread(new FromString(name, contenido), "addEntry.FromString for "
                + name);

    }

    public void addEntry(String name, InputStream is, boolean isPiped) {
        runThread(new FromInputStream(name, is, isPiped),
                "addEntry.FromInputStream for " + name);
    }

    public void addEntry(String name, Reader is) {
        runThread(new FromReader(name, is), "addEntry.FromReader for " + name);
    }

    private void runThread(Runnable r, String name) {
        new Thread(r, threadGroup + " " + name).start();
    }

    public abstract class FromSource implements Runnable {
        public FromSource(){
            waitForThreads(6,null,false);
        }
        protected File getTmpFile() {
            try {
                return File.createTempFile("MTZ", ".tmp");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
    
    public class FromString extends FromSource {
        File tmpFile = null;

        String name = null;

        public void run() {
            addEntry(name, tmpFile);
        }

        public FromString(String name, String content) {
            this.name = name;
            tmpFile = getTmpFile();
            try {
                FileOutputStream fos = new FileOutputStream(tmpFile);
                fos.write(content.getBytes("UTF-8"));
                fos.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public class FromInputStream extends FromSource {
        File tmpFile = null;

        String name = null;

        boolean piped = false;

        InputStream is = null;

        public void run() {
            int readed = 0;
            int wholeReaded = 0;
            byte[] buffer = new byte[1024];
            FileOutputStream fos;
            try {
                fos = new FileOutputStream(tmpFile);
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
                return;
            }
            try {
                while ((readed = is.read(buffer)) > -1) {
                    fos.write(buffer, 0, readed);
                    wholeReaded += readed;
                    if (wholeReaded > 5 * 1024 * 1024
                            && wholeReaded % (5 * 1024 * 1024) < 1024) {
                        log.log("Downloading " + name + " " + wholeReaded
                                / (1024 * 1024) + " MBytes...");
                    }
                }
                log.log("Downloaded " + name + " " + wholeReaded + " Bytes.");
            } catch (IOException e) {
                if (!piped) {
                    e.printStackTrace();
                }
            } finally {
                try {
                    fos.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            }

            addEntry(name, tmpFile);
        }

        public FromInputStream(String name, InputStream is, boolean piped) {
            this.name = name;
            this.is = is;
            this.piped = piped;
            tmpFile = getTmpFile();
        }
    }

    public class FromReader extends FromSource {
        File tmpFile = null;

        String name = null;

        Reader is = null;

        public void run() {
            int readed = 0, wholeReaded = 0;
            char[] buffer = new char[1024];
            FileOutputStream fos;
            try {
                fos = new FileOutputStream(tmpFile);
            } catch (Exception e1) {
                e1.printStackTrace();
                return;
            }
            try {
                while ((readed = is.read(buffer)) > -1) {
                    wholeReaded += readed;
                    if (wholeReaded > 5 * 1024 * 1024
                            && wholeReaded % (5 * 1024 * 1024) < 1024) {
                        log.log("Downloading " + name + " " + wholeReaded
                                / (1024 * 1024) + " MBytes...");
                    }
                    byte[] wr = new String(buffer, 0, readed).getBytes("UTF-8");
                    fos.write(wr, 0, wr.length);
                }
                log.log("Downloaded " + name + " " + wholeReaded + " Bytes.");
            } catch (IOException e) {
                if (!name.endsWith("/Schema.xml")
                        && !name.endsWith("/InternalSchema.xml")) {
                    e.printStackTrace();
                }
            } finally {
                try {
                    fos.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            }
            if (wholeReaded < 40) {
                log.log("Collection " + name + " is Empty.");
            } else {
                addEntry(name, tmpFile);
            }
        }

        public FromReader(String name, Reader is) {
            this.name = name;
            this.is = is;
            tmpFile = getTmpFile();
        }
    }

    private synchronized void addEntry(String name, File f) {
        FileInputStream fis = null;
        ZipEntry zent = new ZipEntry(name);
        try {
            zos.putNextEntry(zent);
            fis = new FileInputStream(f);
            int readed = 0;
            while ((readed = fis.read(buf)) > -1) {
                zos.write(buf, 0, readed);
            }
            zos.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fis.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            f.delete();
        }
        log.log("Saved to Zip:" + name);
    }

    public void close() throws Exception {
        do{
            waitForThreads(0, "Waiting for threads to close zip file.", true);
        }while(numThreadWaiting>0);
        addEntry("unload.log", log.closeAndGetFile());
        zos.close();
        System.out.println("zip file closed.");
    }

    /**
     *  
     */
    private void waitForThreads(int max, String msg, boolean dump) {
        ++numThreadWaiting;
        for (int nt = ThreadMonitor.countThreads(threadGroup),oldnt=nt; 
             nt >= max; 
             nt = ThreadMonitor.countThreads(threadGroup)) {
            long now = System.currentTimeMillis();
            while (System.currentTimeMillis() - now < 2000)
                Thread.yield();
            if(oldnt!=nt){
                oldnt=nt;
	            if (msg != null)
	                log.log(msg + "\n\tcurrent=" + nt + ", max=" + max + ", waiting="+(numThreadWaiting-1));
	            if (dump)
	                ThreadMonitor.dumpThreads(log, threadGroup);
            }
            if(nt==0)
                break;
        }
        --numThreadWaiting;
    }
    
    public int getNumThreadsWaiting(){
        return numThreadWaiting;
    }
}
