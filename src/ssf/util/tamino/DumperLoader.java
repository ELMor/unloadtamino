/*
 * Created on 29-mar-2005
 *
 */
package ssf.util.tamino;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PipedReader;
import java.io.PipedWriter;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import org.xml.sax.SAXException;
import org.xmldb.api.base.Database;
import org.xmldb.api.base.XMLDBException;

import com.softwareag.tamino.db.api.accessor.TAccessLocation;
import com.softwareag.tamino.db.api.accessor.TDefineException;
import com.softwareag.tamino.db.api.accessor.TInsertException;
import com.softwareag.tamino.db.api.accessor.TNonXMLObjectAccessor;
import com.softwareag.tamino.db.api.accessor.TQuery;
import com.softwareag.tamino.db.api.connection.TConnection;
import com.softwareag.tamino.db.api.connection.TConnectionFactory;
import com.softwareag.tamino.db.api.connection.TServerNotAvailableException;
import com.softwareag.tamino.db.api.objectModel.TNonXMLObject;
import com.softwareag.tamino.db.api.objectModel.TNonXMLObjectIterator;
import com.softwareag.tamino.db.api.response.TResponse;
import com.softwareag.tamino.db.tools.loader.TlHTTPrequest;
import com.softwareag.tamino.db.tools.loader.TlSAX2;
import com.softwareag.tamino.db.tools.loader.TlVars;
import com.softwareag.xtools.basic.aspect.Collection;
import com.softwareag.xtools.basic.aspect.CollectionAlreadyExistsException;
import com.softwareag.xtools.basic.aspect.ConnectionFailedException;
import com.softwareag.xtools.basic.aspect.DatabaseNotConnectedException;
import com.softwareag.xtools.basic.aspect.SchemaAlreadyExistsException;
import com.softwareag.xtools.basic.dm.AbstractSchemaImpl;
import com.softwareag.xtools.basic.dm.DatabaseImpl;
import com.softwareag.xtools.basic.dm.ServerImpl;
import com.softwareag.xtools.basic.dm.ServerList;
import com.softwareag.xtools.basic.dm.TSD3_DoctypeImpl;
import com.softwareag.xtools.basic.ta.AddCollectionException;
import com.softwareag.xtools.basic.ta.TaInvalidSchemaFormatException;
import com.softwareag.xtools.basic.ta.TaSchemaObject;
import com.softwareag.xtools.basic.ta.TaTimeoutException;
import com.softwareag.xtools.basic.ta.TaWrongSchemaTypeException;

/**
 * @author UF371231
 *  
 */
public class DumperLoader {
    //Ese es el controlador usado para la conexion de la BBDD
    static String driver = "com.softwareag.tamino.xmldb.api.base.TDatabase";

    public Logger logger = null;

    //Parametros de llamada
    Hashtable htPar = new Hashtable();

    Vector collections = new Vector();

    //Marcador para los bulk unload
    private static int bulkNumber = 0;

    public static void main(String[] args) {
        new DumperLoader().process(args);
    }

    public void process(String[] args) {
        chkArgs(args);

        if (getParam("collist") != null) {
            StringTokenizer stk = new StringTokenizer(
                    (String) getParam("collist"), ",");
            while (stk.hasMoreElements()) {
                collections.add(stk.nextToken());
            }
        }

        createLog();

        String cmd = "";
        for (int i = 0; i < args.length; i++) {
            cmd += " " + args[i];
        }
        log("CommandLine:" + cmd);

        if (chkParam("command", "load")) {
            load();
        } else {
            unload();
        }

    }

    private void log(String s) {
        logger.log(s);
    }

    /**
     *  
     */
    private void createLog() {
        logger = new Logger();
    }

    private boolean chkParam(String name) {
        return htPar.get(name) != null;
    }

    private boolean chkParam(String name, String value) {
        return ((String) htPar.get(name)).equalsIgnoreCase(value);
    }

    private String getParam(String name) {
        return (String) htPar.get(name);
    }

    /**
     * @param args
     */
    private void chkArgs(String[] args) {
        String[] argRepresent = { "-c", "-t", "-s", "-d", "-u", "-p", "-z", "--force",
                "-l", "--no-create-collections", "--no-define-schemas", "--dump-threads" };
        String[] argNames = { "command", "type", "srv", "db", "usr", "pwd", "zf",
                "force", "collist", "nocreate", "nodefine", "dumpthreads" };
        int[] argNumParam = { 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0 };
        boolean[] argRequired = { true, true, true, true, true, true, true, false,
                false, false, false, false };
        boolean[] argPresent = { false, false, false, false, false, false, false,
                false, false, false, false, false };
        try {
            for (int i = 0; i < args.length; i++) {
                boolean cmdFound=false;
                for (int j = 0; j < argRepresent.length; j++) {
                    if (argRepresent[j].equalsIgnoreCase(args[i])) {
                        cmdFound=true;
                        argPresent[j] = true;
                        if (argNumParam[j] > 0) {
                            String cnt = "";
                            for (int k = 1; k <= argNumParam[j]; k++) {
                                cnt += args[++i];
                            }
                            htPar.put(argNames[j], cnt);
                        } else {
                            htPar.put(argNames[j], argNames[j]);
                        }
                        break;
                    }
                }
                if(!cmdFound){
                    log(args[i]+" not found...");
                    System.exit(-2);
                }
            }
        } catch (RuntimeException e) {
            help();
            System.exit(-1);
        }
        for (int i = 0; i < argRequired.length; i++)
            if (argRequired[i] && !argPresent[i]) {
                System.out.println("Argumento " + argRepresent[i] + " requerido.");
                help();
                System.exit(-1);
            }
    }

    /**
     * @param srvURL
     * @param DBName
     * @param user
     * @param pwd
     */
    private void unload() {
        MTZipper mtz = null;
        try {
            //MultiThreaded Zipper
            mtz = new MTZipper(getParam("zf"), logger, "MultiThread ZIP");
            Database db = (Database) Class.forName(driver).newInstance();
            //Obtener colecciones de la bd
            Vector colls = getCollections();
            //Vector de vectores con los doctypes npnxml de cada coleccion
            // anterior
            Vector nonXMLCollsDocTypes = new Vector();
            for (int i = 0; i < colls.size(); i++)
                nonXMLCollsDocTypes.add(new Vector());
            unloadSchemas(mtz, colls, nonXMLCollsDocTypes);
            unloadContent(mtz, colls, nonXMLCollsDocTypes);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                mtz.close();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }

    private void load() {
        ZipInputStream zis = null;
        Vector cols;
        try {
            Database db = (Database) Class.forName(driver).newInstance();
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        try {
            loadCollections(getParam("zf"));
        } catch (Exception e1) {
            e1.printStackTrace();
        } finally {
            System.out.println("Log file at " + logger.flog.getAbsolutePath());
        }
    }

    private void loadCollections(String zipName) throws Exception {
        HashMap loaders = new HashMap();
        DatabaseImpl dbi = getDBI();
        JarFile zFile = new JarFile(zipName);
        TConnection tc = TConnectionFactory.getInstance().newConnection(
                getParam("srv") + "/" + getParam("db"));
        Enumeration en = zFile.entries();
        //Primero los esquemas
        while (en.hasMoreElements()) {
            ZipEntry zEntry = (ZipEntry) en.nextElement();
            String fname = zEntry.getName();
            String colname = null;
            try {
                colname = fname.substring(0, fname.indexOf("/"));
            } catch (RuntimeException e) {
                continue;
            }
            if (!collections.isEmpty() && collections.indexOf(colname) == -1)
                continue;
            if (fname.startsWith(colname + "/schema/")) {
                loadDefine(loaders, dbi, zFile, zEntry, colname);
            } else if (fname.endsWith("/InternalSchema.xml")) {
                //Nada
            }
        }
        //Luego la carga de datos
        zFile = new JarFile(zipName);
        en = zFile.entries();
        while (en.hasMoreElements()) {
            ZipEntry zEntry = (ZipEntry) en.nextElement();
            String fname = zEntry.getName();
            String colname = null;
            try {
                colname = fname.substring(0, fname.indexOf("/"));
            } catch (RuntimeException e) {
                continue;
            }
            if (!collections.isEmpty() && collections.indexOf(colname) == -1)
                continue;
            if (fname.startsWith(colname + "/nonxml/")
                    && fname.endsWith("-contentType")) {
                loadNonXMLInstance(zFile, tc, zEntry, fname, colname);
            } else if (fname.startsWith(colname + "/xml/DATA")) {
                //Bulk load
                loadXMLBulk(zFile, zEntry, colname);
            } else if (fname.startsWith(colname + "/xml/")) {
                //single instance load
                loadXMLInstance(loaders, zFile, zEntry, colname);
            }
        }
    }

    private void loadXMLBulk(ZipFile zf, ZipEntry ze, String colname)
            throws Exception {
        TlVars t = new TlVars();
        t.charset = "utf-8";
        t.commandLineStart = true;
        t.defaultDOMCoreJAR = "xercesImpl.jar";
        t.defineS = false;
        t.defStrBufSize = 2000;
        t.doshow = false;
        t.encoding = "utf-8";
        t.encodingHasBeenSet = false;
        t.entdir = "";
        t.fileName = ze.getName();
        t.format = "loadRequest";
        t.getApiVersion = false;
        t.internalDoc = false;
        t.noInoEtc = false;
        t.noInoId = false;
        t.noUpload = false;
        t.parserIncludeComments = true;
        t.parserNameSpaces = true;
        t.parserValidating = false;
        t.preserveIds = false;
        t.progress = false;
        t.query = "/*";
        t.returnDoc = null;
        t.schemOK = false;
        t.showEncoded = false;
        t.uniCodeEncoding = false;
        t.unload = false;
        t.unloadCount = 0;
        t.url = getParam("srv") + "/" + getParam("db") + "/" + colname;
        t.user = null;
        t.virtualDoc = false;
        TlSAX2 tlsax2 = new TlSAX2(t, null);
        tlsax2.init(t);
        log("BulkLoading " + colname + " size (-1 if not known) is " + ze.getSize()
                + " Bytes");
        tlsax2.start(zf.getInputStream(ze));
        log("BulkLoaded " + t.totalElements + " elements to " + colname
                + " from " + ze.getName() + ". Bytes (xml,upload)=(" + t.xmlVolume
                + "," + t.uploadVolume+")\n------------------------------------------------------");
    }

    /**
     * @return
     * @throws Exception
     */
    private DatabaseImpl getDBI() throws Exception {
        DatabaseImpl dbi;
        dbi = new DatabaseImpl(getParam("db"));
        ServerList sl = new ServerList();
        try {
            ServerImpl si = new ServerImpl(sl, new URL(getParam("srv")));
            dbi.setServer(si);
            dbi.setUserID(getParam("usr"));
            dbi.setPassword(getParam("pwd"));
            dbi.connect(getParam("usr"), getParam("pwd"));
        } catch (Exception e4) {
            System.err.println("Cannot connect to" + getParam("srv"));
            throw e4;
        }
        return dbi;
    }

    /**
     * @param zFile
     * @param tc
     * @param zEntry
     * @param nm
     * @param colname
     * @throws IOException
     * @throws TInsertException
     */
    private void loadNonXMLInstance(ZipFile zFile, TConnection tc,
            ZipEntry zEntry, String nm, String colname) throws IOException,
            TInsertException {
        TNonXMLObjectAccessor nxoa;
        int startDT = (colname + "/nomxml/").length();
        int stopDT = nm.indexOf("/", startDT);
        String docType = nm.substring(startDT, stopDT);
        int startDN = stopDT + 1;
        int stopDN = nm.length() - "-contentType".length();
        String docName = nm.substring(startDN, stopDN);
        String contentType = readIS(zFile.getInputStream(zEntry));
        ZipEntry zec = new ZipEntry(nm.substring(0, nm.length()
                - "-contentType".length()));
        InputStream isc = zFile.getInputStream(zec);
        nxoa = tc.newNonXMLObjectAccessor(TAccessLocation.newInstance(colname));
        log("Inserting ... " + nm);
        nxoa.insert(TNonXMLObject.newInstance(isc, colname, docType, docName,
                contentType));
    }

    /**
     * @param loaders
     * @param zFile
     * @param zEntry
     * @param colname
     * @throws IOException
     * @throws TaTimeoutException
     */
    private void loadXMLInstance(HashMap loaders, ZipFile zFile,
            ZipEntry zEntry, String colname) throws IOException,
            TaTimeoutException {
        Collection c = (Collection) loaders.get(colname);
        InputStream is = zFile.getInputStream(zEntry);
        log("Inserting ... " + zEntry.getName());
        try {
            c.insertXMLInstance(is, null);
        } catch (TInsertException e) {
            System.err.println("Ignoring exception " + e);
        }
    }

    /**
     * @param loaders
     * @param dbi
     * @param zf
     * @param ze
     * @param cn
     * @throws DatabaseNotConnectedException
     * @throws AddCollectionException
     * @throws TaTimeoutException
     * @throws TaInvalidSchemaFormatException
     * @throws TDefineException
     * @throws TaWrongSchemaTypeException
     * @throws IOException
     */
    private void loadDefine(HashMap loaders, DatabaseImpl dbi, ZipFile zf,
            ZipEntry ze, String cn) throws DatabaseNotConnectedException,
            AddCollectionException, TaTimeoutException,
            TaInvalidSchemaFormatException, TDefineException,
            TaWrongSchemaTypeException, IOException {
        Collection c=null;
        //Hay que crear la coleccion y definir el esquema
        if (!chkParam("nocreate")) {
            log("Creating collection " + cn);
            boolean created = false;
            while (!created) {
                try {
                    c = dbi.createCollection(cn);
                    created = true;
                } catch (CollectionAlreadyExistsException e) {
                    created = false;
                    c = dbi.getCollection(cn);
                    log("Existing collection " + cn);
                    if (chkParam("force")) {
                        try {
                            log("... deleting");
                            c.deleteWithContent();
                        } catch (Exception e2) {
                            e2.printStackTrace();
                            log("Could not delete. Not redefining.");
                            created=true;
                        }
                    } else {
                        created=true;
                        log("Not redefining.");
                    }
                }
            }
        }else{
            c = dbi.getCollection(cn);
            if(c==null){
                log("Collection "+c+" not exists, and nocreate option specified. Aborting.");
                System.exit(-1);
            }
        }
        if(!chkParam("nodefine")){
	        log("Creating schema for " + cn);
	        try {
	            c.defineSchema(zf.getInputStream(ze));
	        } catch (SchemaAlreadyExistsException e1) {
	            log("Schema already exists. Trying to redefine...");
	            TaSchemaObject tas = new TaSchemaObject(zf.getInputStream(ze));
	            try {
	                c.updateSchema(tas);
	            } catch (Exception e2) {
	                log("Exception:" + e2 + " Ignoring...");
	            }
	        }
        }
        loaders.put(cn, c);
    }

    /**
     * @param zos
     * @param colls
     * @throws IOException
     */
    private void unloadSchemas(MTZipper mtz, Vector colls, Vector nxds)
            throws Exception {
        for (int coln = 0; coln < colls.size(); coln++) {
            Collection col = (Collection) colls.get(coln);

            String colName = col.getCollectionName();
            Iterator its = col.getSchemas();
            while (its.hasNext()) {
                AbstractSchemaImpl sch = (AbstractSchemaImpl) its.next();
                Vector v = (Vector) nxds.get(coln);
                v.addAll(dumpSchema(mtz, colName, sch));
            }
        }
    }

    /**
     * @param zos
     * @param colName
     * @param sch
     * @throws IOException
     */
    private Vector dumpSchema(MTZipper mtz, String colName,
            AbstractSchemaImpl sch) throws IOException, InterruptedException {
        String schName = sch.getSchemaName();
        String zen = colName;
        Vector cnt = new Vector();
        if (!schName.equals("xp:internalSchema")) {
            zen += "/schema/" + schName;
            //Comprobar los doctypes nonxml
            Iterator nxd = sch.getNonXMLDoctypes();
            while (nxd.hasNext()) {
                TSD3_DoctypeImpl dt = (TSD3_DoctypeImpl) nxd.next();
                cnt.add(dt.getDoctypeName());
            }
        } else {
            zen += "/InternalSchema.xml";
        }
        //Crear el pipe para evitar intermediate storage
        PipedOutputStream pos = new PipedOutputStream();
        PipedInputStream pis = new PipedInputStream();
        pis.connect(pos);
        OutputStreamWriter osw = new OutputStreamWriter(pos, "UTF-8");
        new Thread(new SchemaDumper(sch, osw),"SchemaDumper for "+colName).start();
        mtz.addEntry(zen, pis, true);
        return cnt;
    }

    public class SchemaDumper implements Runnable {
        AbstractSchemaImpl sch = null;

        OutputStreamWriter osw = null;

        public void run() {
            try {
                sch.store(osw);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public SchemaDumper(AbstractSchemaImpl sch, OutputStreamWriter osw)
                throws InterruptedException {
            this.sch = sch;
            this.osw = osw;

        }
    }

    /**
     * @param zos
     * @param colls
     * @throws XMLDBException
     * @throws IOException
     * @throws Exception
     * @throws InterruptedException
     */
    private void unloadContent(MTZipper mtz, Vector colls, Vector nxdt)
            throws XMLDBException, IOException, Exception, InterruptedException {
        for (int coln = 0; coln < colls.size(); coln++) {
            while(mtz.getNumThreadsWaiting()>=6){
                Thread.sleep(2000);
            }
            dumpContent((Collection) colls.get(coln), (Vector) nxdt.get(coln),
                    mtz);
        }
    }

    public class NonXMLDumperWriter implements Runnable {
        TNonXMLObjectAccessor nxoa = null;

        String colName = null;

        Vector docTypes = null;

        MTZipper mtz = null;

        public NonXMLDumperWriter(String colName, Vector docTypes, MTZipper mtz)
                throws Exception {
            this.colName = colName;
            this.docTypes = docTypes;
            this.mtz = mtz;
        }

        public void run() {
            TConnection tc;
            try {
                tc = TConnectionFactory.getInstance().newConnection(
                        getParam("srv") + "/" + getParam("db"));
            } catch (TServerNotAvailableException e1) {
                e1.printStackTrace();
                return;
            }
            nxoa = tc.newNonXMLObjectAccessor(TAccessLocation
                    .newInstance(colName));
            for (int i = 0; i < docTypes.size(); i++) {
                try {
                    String doctype = (String) docTypes.get(i);
                    TQuery tq = TQuery.newInstance(doctype);
                    TResponse tr = nxoa.query(tq);
                    TNonXMLObjectIterator it = tr.getNonXMLObjectIterator();
                    while (it.hasNext()) {
                        TNonXMLObject obj = it.next();
                        String en = obj.getCollection() + "/nonxml/"
                                + obj.getDoctype() + "/" + obj.getDocname();
                        mtz.addEntry(en, obj.getInputStream(), false);
                        en += "-contentType";
                        mtz.addEntry(en, obj.getContentType());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * @param ServerUrl
     * @param DBName
     * @param user
     * @param pwd
     * @param collectionNames
     * @param zos
     * @param db
     * @param coln
     * @throws XMLDBException
     * @throws IOException
     */
    private void dumpContent(Collection col, Vector nxd, MTZipper mtz)
            throws XMLDBException, IOException, Exception {
        String colURL = getParam("srv") + "/" + getParam("db") + "/"
                + col.getCollectionName();
        PipedWriter pw = new PipedWriter();
        PipedReader pr = new PipedReader(pw);
        ContentDumper dumper = new ContentDumper(colURL, pw);
        ContentWriter writer = new ContentWriter(col.getCollectionName(), mtz,
                pr);
        new Thread(dumper,"ContentDumper for "+colURL).start();
        new Thread(writer,"ContentWriter for "+colURL).start();
        if (nxd.size() > 0) {
            new Thread(
                    new NonXMLDumperWriter(col.getCollectionName(), nxd, mtz),"NonXMLDumperWriter for "+colURL)
                    .start();
        }
    }

    /**
     * @param colName
     * @param zos
     * @throws FileNotFoundException
     * @throws IOException
     */
    public class ContentWriter implements Runnable {
        String colName;

        MTZipper mtz;

        PipedReader rend;

        public ContentWriter(String colName, MTZipper mtz, PipedReader rend) {
            this.colName = colName;
            this.mtz = mtz;
            this.rend = rend;
        }

        public void run() {
            try {
                if (chkParam("type", "single"))
                    doJobSingleMode();
                else
                    doJobBulkMode();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void doJobBulkMode() throws IOException {
            mtz.addEntry(colName + "/xml/DATA" + (++bulkNumber), rend);
        }

        /**
         * @throws FileNotFoundException
         * @throws IOException
         */
        private void doJobSingleMode() throws FileNotFoundException,
                IOException {
            int docNumber = 0;
            boolean eof = false, chunkExhausted = true, iniObj = false;
            char[] ck1 = new char[16 * 1024], ck2 = new char[16 * 1024];
            int ini = 0, fin = 0;
            StringBuffer sbc = null;
            String s1 = null, s2 = null, s = null;
            int readed = rend.read(ck2);
            if (readed == -1)
                eof = true;
            s2 = new String(ck2, 0, readed < 0 ? 0 : readed);
            s = s2;
            String header = s2.substring(0, 2 + s2.indexOf("?>"));
            while (!eof || !chunkExhausted) {
                if (chunkExhausted) {
                    if (eof)
                        break;
                    chunkExhausted = false;
                    ck1 = ck2;
                    s1 = s2;
                    readed = rend.read(ck2);
                    if (readed == -1) {
                        eof = true;
                        ck2 = new char[0];
                    }
                    s2 = new String(ck2, 0, readed < 0 ? 0 : readed);
                    s = s1 + s2;
                }
                if (iniObj) {//Se ha detectado inicio de objeto
                    fin = s1.indexOf("</ino:object>", ini);
                    if (fin == -1) {
                        //Es posible que este a caballo entre ambos chunks
                        //o bien que todo el chunk pertenezca al contenido...
                        fin = s.indexOf("</ino:object>", ini);
                        if (fin == -1) {
                            //Todo s1 pertenece al objeto
                            sbc.append(s1.substring(ini));
                            chunkExhausted = true;
                            ini = 0;
                            fin = 0;
                        } else {
                            sbc.append(s.substring(ini, fin));
                            ini = fin - s1.length();
                            fin = 0;
                            //Rotamos chunks hasta fin
                            ck1 = ck2;
                            s1 = s2;
                            readed = rend.read(ck2);
                            if (readed == -1) {
                                eof = true;
                                ck2 = new char[0];
                            }
                            s2 = new String(ck2, 0, readed < 0 ? 0 : readed);
                            s = s1 + s2;
                            iniObj = false;
                            mtz.addEntry(
                                    colName + "/xml/" + docNumber + ".xml", sbc
                                            .toString());
                        }
                    } else {
                        sbc.append(s1.substring(ini, fin));
                        ini = fin + 13;
                        iniObj = false;
                        mtz.addEntry(colName + "/xml/" + docNumber + ".xml",
                                sbc.toString());
                    }
                } else {
                    if (!(ini >= s1.length())) {
                        ini = s1.indexOf("<ino:object", ini);
                        if (ini == -1) { //Es posible que este a caballo entre
                            // ambos chunks...
                            ini = s.indexOf("<ino:object", ini);
                            if (ini == -1) {
                                chunkExhausted = true;
                                ini = 0;
                                fin = 0;
                                continue;
                            }
                            ini = ini - s1.length();
                            //Rotamos chunks...
                            ck1 = ck2;
                            s1 = s2;
                            readed = rend.read(ck2);
                            if (readed == -1) {
                                eof = true;
                                ck2 = new char[0];
                            }
                            s2 = new String(ck2, 0, readed < 0 ? 0 : readed);
                            s = s1 + s2;
                        }
                        ini = s.indexOf(">", ini) + 1; //Obviamos
                        if (ini > s1.length()) {
                            chunkExhausted = true;
                            ini -= s1.length();
                        }
                        // <ino:object...>
                        iniObj = true;
                        sbc = new StringBuffer();
                        sbc.append(header);
                        docNumber++;
                    } else {
                        chunkExhausted = true;
                    }
                }
            }
        }
    }

    /**
     * @param colURL
     * @param user
     * @param pwd
     * @param file
     * @throws Exception
     * @throws SAXException
     * @throws FileNotFoundException
     */
    public class ContentDumper implements Runnable {

        String colURL;

        PipedWriter wend = null;

        public ContentDumper(String url, PipedWriter rend) throws Exception {
            colURL = url;
            wend = rend;
        }

        public void run() {
            doJob();
        }

        /**
         * @throws Exception
         * @throws SAXException
         */
        private void doJob() {
            TlVars tlv = new TlVars();
            initTlVar(tlv);
            tlv.doshow = false;
            TlHTTPrequest tlh = new TlHTTPrequest(tlv);
            String s = null;
            s = tlv.encoding;
            try {
                tlh.init(false);
                String s1 = "_encoding=" + tlv.encoding + "&_XQL=" + encodeForHttp(s, tlv.query);
                tlh.write(tlv.format, tlv.encoding, s1, wend);
            } catch (SAXException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        /**
         * @param tlv
         */
        public void initTlVar(TlVars tlv) {
            tlv.charset = tlv.encoding;
            tlv.url = colURL;
            tlv.user = getParam("usr");
            tlv.passwd = getParam("pwd");
            tlv.noUpload = true;
            tlv.unload = true;
            tlv.noInoId = true;
            tlv.showEncoded = true;
            tlv.encoding = "UTF-8";
        }

    }

    /**
     * @param ServerUrl
     * @param DBName
     * @param user
     * @param pwd
     * @param collections
     * @throws MalformedURLException
     * @throws ConnectionFailedException
     */
    private Vector getCollections() throws Exception {
        Vector cs = new Vector();
        DatabaseImpl dbi = getDBI();
        getCollections(dbi, cs);
        return cs;
    }

    /**
     * @param dbi
     * @param v
     */
    private void getCollections(DatabaseImpl dbi, Vector v) {
        Enumeration itk = dbi.children();
        for (; itk.hasMoreElements();) {
            Collection c1 = (Collection) itk.nextElement();
            String colName = c1.getCollectionName();

            if (!colName.startsWith("ino:")) {
                if (collections.isEmpty())
                    v.add(c1);
                else if (collections.indexOf(colName) != -1)
                    v.add(c1);
                else
                    log("Ignoring collection " + colName);
            }
        }
    }

    /**
     *  
     */
    private void help() {
        System.out
                .println("Argumentos deben ser\n"
                        + "\t-c [load|unload]       \treq\tComando {unload}\n"
                        + "\t-t [bulk|single]       \treq\tTipo de carga/descarga {bulk}.\n"
                        + "\t-z <ZipFile>           \treq\tArchivo comprimido donde cargar/descargar datos\n"
                        + "\t-s <server>            \treq\tServidor Tamino\n"
                        + "\t-d <database>          \treq\tBase de Datos\n"
                        + "\t-u <user>              \treq\tUsuario\n"
                        + "\t-p <pwd>               \treq\tPassword\n"
                        + "\t-l <c1,c2...>          \topt\tLista de colecciones\n"
                        + "\t--no-create-collections\topt\tNo crea colecciones\n"
                        + "\t--no-define-schemas    \topt\tNo define esquemas\n"
                        + "\t--force                \topt\tSi existe coleccion la borra y la crea\n"
                        + "\nEjemplo:"
                        + "\njava -jar tdl.jar -c unload -t bulk -s http://localhost/tamino -d MyDB -u admin -p pwd -z h1.zip");
    }

    public static String encodeForHttp(String s, String s1) throws Exception {
        StringBuffer stringbuffer = new StringBuffer();
        byte abyte0[] = s1.getBytes(s);
        for (int i = 0; i < abyte0.length; i++) {
            int j;
            if (abyte0[i] < 0)
                j = abyte0[i] & 0xff;
            else
                j = abyte0[i];
            stringbuffer.append(int2Hex(j));
        }

        return stringbuffer.toString();
    }

    public static String int2Hex(int i) {
        StringBuffer stringbuffer = new StringBuffer("%");
        char c = Character.forDigit(i & 0xf, 16);
        char c1 = Character.forDigit(i >>> 4 & 0xf, 16);
        stringbuffer.append(c1);
        stringbuffer.append(c);
        return stringbuffer.toString();
    }

    public static String readIS(InputStream is) throws IOException {
        char[] cbu = new char[1024];
        int readed = 0;
        Reader r = new InputStreamReader(is);
        StringBuffer sb = new StringBuffer();
        while (-1 != (readed = r.read(cbu))) {
            sb.append(cbu, 0, readed);
        }
        r.close();
        return sb.toString();
    }

}
