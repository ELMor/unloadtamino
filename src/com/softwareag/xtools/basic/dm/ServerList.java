// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package com.softwareag.xtools.basic.dm;

import com.softwareag.common.instrumentation.logging.Logger;
import com.softwareag.common.instrumentation.logging.LoggerFactory;
import com.softwareag.common.instrumentation.logging.LoggerUtil;
import com.softwareag.xtools.basic.aspect.Database;
import com.softwareag.xtools.basic.aspect.Locatable;
import com.softwareag.xtools.basic.aspect.Server;
import com.softwareag.xtools.basic.aspect.Universe;
import com.softwareag.xtools.basic.ui.IconCache;
import com.softwareag.xtools.basic.ui.IconConstants;
import com.softwareag.xtools.basic.ui.ResourceBundleHelper;
import com.softwareag.xtools.common.framework.aspect.Aspect;
import com.softwareag.xtools.common.framework.aspect.BeanContextAware;
import com.softwareag.xtools.common.framework.aspect.io.Externalizable;
import com.softwareag.xtools.common.framework.aspect.io.Internalizable;
import com.softwareag.xtools.common.framework.aspect.io.MaterialInput;
import com.softwareag.xtools.common.framework.aspect.io.MaterialInputException;
import com.softwareag.xtools.common.framework.aspect.io.MaterialOutput;
import com.softwareag.xtools.common.framework.aspect.io.MaterialOutputException;
import com.softwareag.xtools.common.framework.component.GenericBeanContextChild;

import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.beans.beancontext.BeanContext;
import java.beans.beancontext.BeanContextChild;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.swing.Icon;

// Referenced classes of package com.softwareag.xtools.basic.dm:
//            AbstractMutableNode, ServerImpl

public class ServerList extends AbstractMutableNode
    implements Universe, BeanContextAware, Externalizable, Internalizable
{

    private static final String LOG_NAME = LoggerUtil.getThisClassName();
    private static Logger logger = LoggerFactory.getLogger(LOG_NAME);
    protected static final String RESOURCES = "com.softwareag.xtools.basic.dm.resources.serverlistresources";
    private static final String REPRESENTATION_NAME = "REPRESENTATION_NAME";
    private static final String SERVER_IDENTIFIER = "server";
    public static final String IDENTIFIER_NAME = "identifier";
    protected static Set availableAspects;
    private String identifier;
    private static Icon icon = null;
    private String representationName;
    private GenericBeanContextChild gbcc;

    public ServerList()
    {
        identifier = null;
        representationName = ResourceBundleHelper.getStringResource("com.softwareag.xtools.basic.dm.resources.serverlistresources", "REPRESENTATION_NAME", "");
        gbcc = null;
        createIdentifier();
    }

    public String getIdentifier()
    {
        return identifier;
    }

    private void createIdentifier()
    {
        try
        {
            Thread.sleep(10L);
        }
        catch(InterruptedException interruptedexception)
        {
            Thread.currentThread().interrupt();
        }
        identifier = "universe_" + System.currentTimeMillis();
    }

    public Server getServer(URL url)
    {
        Server server = null;
        Iterator iterator = iterator();
        Object obj = null;
        while((server == null) & iterator.hasNext()) 
        {
            Object obj1 = iterator.next();
            if((obj1 instanceof Server) && ((Server)obj1).hasLocationURL(url))
                server = (Server)obj1;
        }
        return server;
    }

    public Server getServer(URL url, boolean flag)
    {
        Object obj = getServer(url);
        if((obj == null) & flag)
        {
            obj = new ServerImpl(this, url);
            insertSortedByRepresentationName((ServerImpl)obj);
        }
        return ((Server) (obj));
    }

    public boolean hasServer(URL url)
    {
        return getServer(url) != null;
    }

    public Iterator getServers()
    {
        return iterator();
    }

    public Iterator getServerImpls()
    {
        return getServers();
    }

    public Icon getIcon()
    {
        return icon;
    }

    public String getRepresentationName()
    {
        return representationName;
    }

    public void setBeanContextChild(GenericBeanContextChild genericbeancontextchild)
    {
        gbcc = genericbeancontextchild;
    }

    public GenericBeanContextChild getBeanContextChild()
    {
        if(gbcc==null){
            gbcc=new GenericBeanContextChild();
        }
        return gbcc;
    }

    public Aspect getAspect(Class class1)
    {
        if(class1.isAssignableFrom(getClass()))
            return this;
        else
            return null;
    }

    public Set getAvailableAspectTypes()
    {
        return availableAspects;
    }

    public boolean hasAspect(Class class1)
    {
        return getAvailableAspectTypes().contains(class1);
    }

    public boolean isSame(Aspect aspect)
    {
        return equals(aspect);
    }

    public void writeTo(MaterialOutput materialoutput)
        throws MaterialOutputException
    {
        materialoutput.writeObjectList("server", getServerImpls());
        materialoutput.writeObjectAttribute("identifier", identifier);
    }

    public void readFrom(MaterialInput materialinput)
        throws MaterialInputException
    {
        identifier = materialinput.readObjectAttribute("identifier");
        ServerImpl serverimpl;
        for(Iterator iterator = materialinput.readObjectList("server"); iterator.hasNext(); insertSortedByRepresentationName(serverimpl))
        {
            serverimpl = (ServerImpl)iterator.next();
            serverimpl.setUniverse(this);
        }

    }

    public boolean equalsUniverseAspect(Universe universe)
    {
        if(universe == null)
            return false;
        for(Iterator iterator = universe.getServers(); iterator.hasNext();)
            if(!hasServer(((Server)iterator.next()).getLocationURL()))
                return false;

        for(Iterator iterator1 = getServers(); iterator1.hasNext();)
            if(!universe.hasServer(((Server)iterator1.next()).getLocationURL()))
                return false;

        return true;
    }

    public Database getDatabase(String s, String s1)
    {
        Server server = null;
        Database database = null;
        try
        {
            server = getServer(new URL(s), true);
        }
        catch(MalformedURLException malformedurlexception)
        {
            return null;
        }
        if(server != null)
            database = server.getDatabase(s1, true);
        return database;
    }

    public String toString()
    {
        StringBuffer stringbuffer = new StringBuffer();
        stringbuffer.append(getClass().getName());
        stringbuffer.append("[");
        stringbuffer.append("servers=[");
        for(Iterator iterator = getServerImpls(); iterator.hasNext();)
        {
            stringbuffer.append(iterator.next().toString());
            if(iterator.hasNext())
                stringbuffer.append(", ");
        }

        stringbuffer.append("]");
        stringbuffer.append("]");
        return stringbuffer.toString();
    }

    static Class _mthclass$(String s)
    {
        try
        {
            return Class.forName(s);
        }
        catch(ClassNotFoundException classnotfoundexception)
        {
            throw new NoClassDefFoundError(classnotfoundexception.getMessage());
        }
    }

    static 
    {
        availableAspects = null;
        availableAspects = new HashSet();
        availableAspects.add(com.softwareag.xtools.basic.aspect.Universe.class);
        availableAspects.add(com.softwareag.xtools.basic.aspect.MutableNode.class);
        availableAspects.add(com.softwareag.xtools.common.framework.aspect.BeanContextAware.class);
        availableAspects.add(com.softwareag.xtools.common.framework.aspect.Aspect.class);
        availableAspects = Collections.unmodifiableSet(availableAspects);
        icon = IconCache.getTheCache().getIcon(IconConstants.EARTH_ICON);
    }
}
