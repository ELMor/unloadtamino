// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package com.softwareag.tamino.db.tools.loader;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PipedWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.MessageFormat;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

// Referenced classes of package com.softwareag.tamino.db.tools.loader:
//            SAXQueryToMassLoad, TlVars, TaminoLoad, TlResource, 
//            SAXCopyClass

public final class TlHTTPrequest
{

    private TlVars tlvars;
    private DataOutputStream out;
    private HttpURLConnection con;
    private URL url;
    StringBuffer result;

    public TlHTTPrequest(TlVars tlvars1)
    {
        tlvars = tlvars1;
    }

    private static final byte[] getpass(String s)
    {
        int i = s.length();
        byte abyte0[] = new byte[((i + 2) / 3) * 4];
        byte abyte1[] = (s + "\000\0").getBytes();
        int j = 0;
        for(int k = 0; k < i; k += 3)
        {
            abyte0[j++] = base64((abyte1[k] & 0xfc) >> 2);
            abyte0[j++] = base64((abyte1[k] & 3) << 4 | (abyte1[k + 1] & 0xf0) >> 4);
            abyte0[j++] = base64((abyte1[k + 1] & 0xf) << 2 | (abyte1[k + 2] & 0xc0) >> 6);
            abyte0[j++] = base64(abyte1[k + 2] & 0x3f);
        }

        i %= 3;
        switch(i)
        {
        case 1: // '\001'
            abyte0[--j] = 61;
            // fall through

        case 2: // '\002'
            abyte0[--j] = 61;
            // fall through

        default:
            return abyte0;
        }
    }

    private static final byte base64(int i)
    {
        if(i < 26)
            return (byte)(i + 65);
        if(i < 52)
            return (byte)((i - 26) + 97);
        if(i < 62)
            return (byte)((i - 52) + 48);
        if(i == 62)
            return 43;
        if(i == 63)
            return 47;
        else
            throw new RuntimeException("Bad integer to base64: " + i);
    }

    public boolean init(boolean flag)
        throws Exception
    {
        try
        {
            String s = tlvars.url;
            int i = s.lastIndexOf('/');
            String s2 = s.substring(i + 1);
            String s3 = s.substring(0, i + 1);
            s = s3 + TaminoLoad.encodeForHttp("utf-8", s2);
            url = new URL(s);
            con = (HttpURLConnection)url.openConnection();
            con.setRequestProperty("User-agent", "TaminoLoad");
            con.setRequestProperty("content-type", "application/x-www-form-urlencoded");
            con.setRequestProperty("accept-language", "en");
            con.setRequestProperty("Accept-Charset", tlvars.charset);
            if(tlvars.user != null && tlvars.passwd != null)
                con.setRequestProperty("Authorization", "Basic " + new String(getpass(tlvars.user + ":" + tlvars.passwd)));
            con.setUseCaches(false);
            con.setDoInput(true);
            con.setDoOutput(true);
        }
        catch(Exception exception)
        {
            String s1 = TlResource.getMessage("TlInitErr");
            System.err.println(MessageFormat.format(s1, new String[] {
                exception.toString()
            }));
            throw new Exception();
        }
        return true;
    }

    public String write(String s)
        throws SAXException
    {
        try
        {
            PrintWriter printwriter = new PrintWriter(new OutputStreamWriter(con.getOutputStream()));
            printwriter.print(s);
            printwriter.close();
        }
        catch(Exception exception)
        {
            String s1 = TlResource.getMessage("TlHTTPErr");
            throw new SAXException(MessageFormat.format(s1, new String[] {
                exception.toString()
            }));
        }
        read();
        return result.toString();
    }

    private void read()
        throws SAXException
    {
        try
        {
            BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(con.getInputStream(), tlvars.encoding));
            result = new StringBuffer();
            int i;
            while((i = bufferedreader.read()) != -1) 
                result.append((char)i);
            bufferedreader.close();
        }
        catch(Exception exception)
        {
            String s = TlResource.getMessage("TlHTTPReadErr");
            throw new SAXException(MessageFormat.format(s, new String[] {
                exception.toString()
            }));
        }
    }

    public void write(String s, String s1, String s2, PipedWriter pipe)
        throws SAXException
    {
        try
        {
            if(tlvars.doshow)
            {
                System.out.println(TlResource.getMessage("TlHTTPedoc"));
                System.out.println(s2);
            }
            PrintWriter printwriter = new PrintWriter(new OutputStreamWriter(con.getOutputStream()));
            printwriter.print(s2);
            printwriter.close();
        }
        catch(Exception exception)
        {
            String s3 = TlResource.getMessage("TlHTTPErr");
            throw new SAXException(MessageFormat.format(s3, new String[] {
                exception.toString()
            }));
        }
        try
        {
            BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(con.getInputStream(), s1));
            BufferedWriter bufferedwriter = new BufferedWriter(pipe);
            int i;
            if(s == "queryResult")
            {
                while((i = bufferedreader.read()) != -1) 
                    bufferedwriter.write((char)i);
            } else
            {
                SAXQueryToMassLoad saxquerytomassload = new SAXQueryToMassLoad(new InputSource(bufferedreader), bufferedwriter, tlvars);
                bufferedwriter.write("<?xml version=\"1.0\" encoding=\"" + s1 + "\" ?>");
                saxquerytomassload.init();
                saxquerytomassload.start();
            }
            bufferedreader.close();
            bufferedwriter.flush();
            bufferedwriter.close();
        }
        catch(Exception exception1)
        {
            String s4 = TlResource.getMessage("TlHTTPReadErr");
            throw new SAXException(MessageFormat.format(s4, new String[] {
                exception1.toString()
            }));
        }
        if(tlvars.progress && !tlvars.format.equals("queryResult"))
        {
            if(tlvars.unloadCount != 0)
                tlvars.unloadCount--;
            System.out.println("\n\t" + tlvars.unloadCount + " objects unloaded");
        }
    }
}
